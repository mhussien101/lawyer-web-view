import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var title = 'mohamed website';  // TODO : replace this value with your app name
    const PrimaryColor = const Color(0xFF86597b);
    var websiteUrl = "https://alialhinai.com/";  // TODO : replace this Link with your website link
    return new MaterialApp(
        title: title,
        routes: {
          '/widget': (_) => new WebviewScaffold(
            url: websiteUrl,
            appBar: new PreferredSize(
                preferredSize: Size.fromHeight(0),
               child:  AppBar(
               backgroundColor: PrimaryColor
             )
            ),
            withZoom: false,
            withLocalStorage: true,
          )
        },
        home: new MyAppHomePage(),
    );
  }
}

class MyAppHomePage extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}


class _MyAppState extends State<MyAppHomePage> {
  void _openWebPage() {
    Navigator.of(context).pushNamed('/widget');
  }

 @override
  void initState() {
    super.initState();
 new Future.delayed(
        const Duration(seconds: 1),
        () => _openWebPage() );

}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    );
  }

}